#include <iostream>
#include <string>
#include <cstring>

using namespace std;

/*
int testhash(string w)
{
    int h = 0;
    for(uint i=0; i<w.length(); i++)
    {
        h=h*37+w.at(i);
    }
    return abs(h);
}

string word(uint wordnum, uint charcount)
{
    string result;
    for(uint i = 0; i < charcount; i++)
    {
        char nextChar = 65 + (wordnum % 26);
        result.insert(0, 1, nextChar);
        wordnum /=26;
    }
    return result;
}
*/

//Rewritten for performance
inline int testhash(char* w, uint size, uint x=37)
{
    int h = 0;
    for(uint i=0; i<size; i++)
        h=h*x+w[i];
    return abs(h);
}

inline void genword(uint wordnum, uint charcount, char* result)
{
    for(uint i = 0; i < charcount; i++)
    {
        char nextChar = 65 + (wordnum % 26);
        result[charcount - 1 - i] = nextChar;
        wordnum /= 26;
    }
}

int main()
{
    int word_size = 8;
    int maxfilesize = 10000;
    int m = 100000;

    uint current = 0;
    uint csize = 0;
    uint stringsize = word_size+1;

    char* result = (char*) malloc(maxfilesize);
    char* cword = (char*) malloc(word_size + 2);
    cword[word_size+1] = '\0';
    cword[word_size] = '\n';

    while(csize < maxfilesize - stringsize)
    {
        genword(current, word_size, cword);
        current++;
        if( !( testhash(cword, word_size) % m ) )
        {
            //ret.append(cword);
            memcpy( (void*) (result + csize), (void*) cword, stringsize );
            csize += stringsize;
        }
    }

    /*while(ret.size()<999990)
    {
        string cword = word(current, word_size);
        current++;
        if(!(testhash(cword) % 100000))
        {
            ret += cword;
            ret += "\n";
        }
    }*/

    cout << result;

    return 0;
}
