#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <chrono>
#include <cassert>
#include <string>

//#define DEBUG

using namespace std;
using namespace std::chrono;

struct SearchState;
template<typename T> int* construct_searcharray(int count, bool randomize);
template<typename T> int find_lowest_idx(SearchState* state,
                                        T* array,
                                        int array_size,
                                        T num);
template<typename T> int find_highest_idx(SearchState* state,
                                        T* array,
                                        int array_size,
                                        T num);
int test(int N, int retry_num);

struct SearchState
{
    int lbound;
    int ubound;
};

template<typename T> int* construct_searcharray(int count, bool randomize)
{
    T* array = (T*) malloc(count * sizeof(T));
    T  cNum = (T) 0;

    if(randomize) srand(time(NULL));
    else srand(0);

    for(int i = 0; i < count; i++)
    {
        cNum += (T) (rand()%10); //Is a little biased, but so what?
        array[i] = cNum;
#ifdef DEBUG
        cout << cNum << " ";
#endif //DEBUG
    }
#ifdef DEBUG
    cout << endl;
#endif //DEBUG
    return array;
}

/* This function returns the lowest array entry that is greater than or equal
 * to num, i.e. the first entry in the interval
    - returns array_size if num is larger than the last array entry*/
template<typename T> int find_lowest_idx(SearchState* state,
                                        T* array,
                                        int array_size,
                                        T num)
{
    if(num <= array[0]) return 0;
    if(num > array[array_size-1]) return array_size;

    while(state->lbound != state->ubound)
    {
        int r = (int) floor( ( state->lbound + state->ubound ) / 2.0 );
        if(num > array[r]) {
            state->lbound = r + 1;
        } else {
            state->ubound = r;
        }
    }
    return state->ubound;
}

/* This function returns the highest array entry that is smaller than or equal
 * to num, i.e. the last entry of the interval
 *  - returns -1 if num is smaller than the first entry of the array*/
template<typename T> int find_highest_idx(SearchState* state,
                                        T* array,
                                        int array_size,
                                        T num)
{
    if(num < array[0]) return -1;
    if(num > array[array_size-1]) return array_size - 1;

    while(state->lbound != state->ubound)
    {
        int r = (int) ceil( ( state->lbound + state->ubound ) / 2.0 );
        if(num < array[r]) {
            state->ubound = r - 1;
        } else {
            state->lbound = r;
        }
    }
    return state->ubound;
}

int main()
{
    int N,l,r;
    string ch1;
    bool randomize;
    cout << "Bitte wähle ein N (Anzahl der Einträge im Array) ";
    cin >> N;
    assert(N > 0);
    cout << "Bitte wähle eine kleinste Zahl ";
    cin >> l;
    cout << "Bitte wähle eine größte Zahl ";
    cin >> r;
    cout << "Soll das Array mit einem zufälligen Seed initialisiert werden? \
            (yYjJ, default:No) ";
    cin >> ch1;
    switch(ch1.at(0))
    {
        case 'y': randomize = true; break;
        case 'Y': randomize = true; break;
        case 'j': randomize = true; break;
        case 'J': randomize = true; break;
        default: randomize = false;
    }

    assert(l <= r);

    //Construct the array to be searched
    int* array = construct_searcharray<int>(N, randomize);

    SearchState lNum{0,N-1};
    SearchState uNum{0,N-1};

    //Initialise timer
    high_resolution_clock::time_point t1 =
            high_resolution_clock::now();
    //

    int lindex = find_lowest_idx<int>(&lNum, array, N, l);
    int rindex = find_highest_idx<int>(&uNum, array, N, r);

    int count = rindex - lindex + 1;

    //End timer
    high_resolution_clock::time_point t2 =
            high_resolution_clock::now();
    microseconds timer =
            duration_cast<microseconds>(t2 - t1);
    //

    cout << "Es befinden sich "
         << count
         << " Zahlen im Intervall ["
         << l
         << ","
         << r
         << "]" << endl;
    cout << "Die Berechnung hat "
         << timer.count()
         << " Mikrosekunden gebraucht" << endl;

    cout << "Testlauf mit N=10,100,1000,1.000.000,10.000.000" << endl;

    cout << test(10, 100) << " Nanosekunden" << endl
         << test(100, 100) << " Nanosekunden" << endl
         << test(1000, 100) << " Nanosekunden" << endl
         << test(1000000, 100) << " Nanosekunden" << endl
         << test(10000000, 100) << " Nanosekunden" << endl;

    return 0;
}

int test(int N, int retry_num)
{
    int t = 0;

    for(int i = 0; i < retry_num; i++)
    {
        int* array = construct_searcharray<int>(N, true);

        high_resolution_clock::time_point t1 =
                high_resolution_clock::now();

        SearchState lNum{0,N-1};
        SearchState uNum{0,N-1};

        int lindex = find_lowest_idx<int>(&lNum, array, N, N/2);
        int rindex = find_highest_idx<int>(&uNum, array, N, (N*5)/2);

        high_resolution_clock::time_point t2 =
                high_resolution_clock::now();
        nanoseconds timer =
                duration_cast<nanoseconds>(t2 - t1);
        delete[] array;
        t += timer.count();
    }

    return (int) ((double) t) / retry_num;
}
